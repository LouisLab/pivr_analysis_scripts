# PiVR Analysis Scripts

The collection of scripts in this repository are self-contained and 
can be used to analyze data produced with PiVR. 

To run scripts in this repository on your compter follow the 
instructions below:

1. Install Git https://git-scm.com/downloads 
   
2. Install Miniconda on your PC: 
   https://conda.io/miniconda.html.
   
3. After installation, open the anaconda prompt under Windows or a regular terminal under Mac/Linux.   
   
4. Clone the repository by typing in the anaconda prompt/terminal:
   
   `git clone https://gitlab.com/LouisLab/pivr_analysis_scripts`
   
5. Create a new python environment: 
   
   `conda create --name PiVR_analysis_env -y`
   
6. Activate the environment: 

   Use the following command under Windows

   `activate PiVR_analysis_env`

   Use the following command under Mac/Linux

   `source activate PiVR_analysis_env`
 
7. Now you need to install the necessary packages in the environment.
   Copy the commands line by line to your anaconda prompt and hit 
   enter.
   
   `conda install pandas -y`
 
   `conda install matplotlib -y`

   `conda install scipy -y`

   `conda install imageio -y`

8. Congratulations. You are done with the installation. Next, close 
   the anaconda prompt and open another instance of the anaconda prompt.
   
9. Activate the environment

   Use the following command under Windows

   `activate PiVR_analysis_env`

   Use the following command under Mac/Linux

   `source activate PiVR_analysis_env`

10. To run an analysis script that comes with the PiVR package you just downloaded, 
    first change the current directory to where the python script is 
    located. If you followed the instructions, type:
    
    `cd pivr_analysis_scripts\Drosophila_Larva`

    (the folder might need to be changed depending on where you downloaded 'git clone')

11. Now you can run the script from the anaconda prompt/terminal by calling 
    it with python:
    
    `python calculate_larval_speed.py`

12. When asked to specify the folder with the data you want to analyze, pick the folder that contains your folders with 
    the output of individual trials. In other words, you must use a folder than contains at least one folder with your data. 

      



