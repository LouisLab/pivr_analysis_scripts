__author__ = 'David Tadres'
__project__ = 'PiVR analysis scripts'

"""
This script can be used to calculate movement speed of a larva 
tracked with PiVR.

After running the script, you may select a folder with one or more 
experiments tracked with PiVR. 

The x/y and coordinates are first filtered using a Savitzky-Golay 
filter to remove high frequency noise.
Then the speed is calculated over 3 points, i.e. for frame 
i = (i - 1) - (i + 1).

The script directly adds the calculated values to the 'data.csv' file 
in each experimental folder. In addition, it produces a plot showing 
the calculated speed. 
"""

import json
from pathlib import Path
from tkinter import filedialog, Tk
import imageio
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.signal import savgol_filter

def savgol(x_data, y_data, framerate, order):
    """
    Filter X and Y data with Savitzky Golay filter with window size 2
    seconds.

    Note: If window size would be and even number (e.g. at FPS = 2,
    window size would be 4) it is increased by 1 to yield an odd
    window size. This is necessary for the function to work:
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.savgol_filter.html
    """

    window_size = framerate * 2
    if window_size % 2 == 0:
        window_size += 1 # window size must be odd number
    x_filtered = savgol_filter(x_data, window_size, order)
    y_filtered = savgol_filter(y_data, window_size, order)

    return (x_filtered, y_filtered)

def calc_speed(x_filt, y_filt, pixel_per_mm, framerate):
    """
    Calculate Speed of animal.

    Adapted from motion.m from the SOS publication: https://doi.org/10.1371/journal.pone.0041642

    First, distance is calculated over the previous and the next frame.
    dx = x_i-1 - x_i+1
    dy = y_i-1 - y_i+1
    Eucledian distance is just:
    distance = sqrt(dx^2 + dy^2)
    Then, normalize from pixel to mm by dividing by the px/mm factor
    Next, this is a bit unusual as we are integrating over 3 points instead of 2:
    We have to use the symmetric derivative
    https://en.wikipedia.org/wiki/Symmetric_derivative everything needs to be divided
    by 2
    speed = distance / 2 / framerate
    """
    speed = np.zeros((x_filt.shape[0]))
    speed.fill(np.nan)
    for i_frame in range(2, x_filt.shape[0] - 1):
        dx = (x_filt[i_frame + 1]) - (x_filt[i_frame - 1])
        dy = (y_filt[i_frame + 1]) - (y_filt[i_frame - 1])
        distance = np.sqrt(dx ** 2 + dy ** 2)
        distance /= pixel_per_mm
        speed[i_frame] = distance / (2 / framerate)
    return (speed)

root = Tk()
root.withdraw()
filepath = Path(filedialog.askdirectory())

for i_folder in filepath.iterdir():
    if i_folder.is_dir():
        print(i_folder)
        # Read the binary images...
        raw_images  = np.load(Path(i_folder,'sm_thresh.npy'))
        # ...and the coordinates the each image is located...
        bounding_boxes = np.load(Path(i_folder, 'bounding_boxes.npy'))
        # Since there's the suboptimal naming convention when
        # collecting a video of 'data.csv' to denote info about the
        # stimulus and when then doing analysis it's also called
        # 'data.csv' the loop below first collects all data.csv names
        # and selects the newest one (as analysis must have been done
        # after the data collection).
        files_of_interest = []
        for i_file in i_folder.iterdir():
            if 'data.csv' in i_file.name:
                files_of_interest.append(i_file.name)
                # data_name = i
        if len(files_of_interest) == 1:
            data_name = files_of_interest[0]
        else:
            files_of_interest.sort()
            data_name = files_of_interest[-1]
        data_csv = pd.read_csv(Path(i_folder, data_name), sep=',')

        with open((Path(i_folder, 'experiment_settings.json')), 'r') as file:
            experiment_settings = json.load(file)
            pixel_per_mm = experiment_settings['Pixel per mm']
            recording_time = experiment_settings['Recording time']
            recording_fps = experiment_settings['Framerate']

        # read background image
        background_image = imageio.imread(Path(i_folder, 'Background.jpg'))

        # Filter each of the different points of the animal using a
        # Savitzky Golay filter (4th polynomal) ...
        head_x, head_y = savgol(data_csv['X-Head'].dropna(),
                                data_csv['Y-Head'].dropna(),
                                experiment_settings['Framerate'],
                                order=4)
        # ... then calculate the speed
        head_v = calc_speed(head_x, head_y,
                            pixel_per_mm,
                            recording_fps)

        tail_x, tail_y = savgol(data_csv['X-Tail'].dropna(),
                                data_csv['Y-Tail'].dropna(),
                                experiment_settings['Framerate'],
                                order=4)
        tail_v = calc_speed(tail_x, tail_y,
                            pixel_per_mm,
                            recording_fps)

        centroid_x, centroid_y = savgol(data_csv['X-Centroid'].dropna(),
                                        data_csv['Y-Centroid'].dropna(),
                                        experiment_settings['Framerate'],
                                        order=4)
        centroid_v = calc_speed(centroid_x,
                                centroid_x,
                                pixel_per_mm,
                                recording_fps)

        midpoint_x, midpoint_y = savgol(data_csv['X-Midpoint'].dropna(),
                                        data_csv['Y-Midpoint'].dropna(),
                                        experiment_settings['Framerate'],
                                        order=4)
        midpoint_v = calc_speed(midpoint_x,
                                midpoint_y,
                                pixel_per_mm,
                                recording_fps)

        # Save the calculated speed in the data csv
        data_csv['X-Head SavGol'] = pd.Series(head_x)
        data_csv['Y-Head SavGol'] = pd.Series(head_y)

        data_csv['X-Tail SavGol'] = pd.Series(tail_x)
        data_csv['Y-Tail SavGol'] = pd.Series(tail_y)

        data_csv['X-Centroid SavGol'] = pd.Series(centroid_x)
        data_csv['Y-Centroid SavGol'] = pd.Series(centroid_y)

        data_csv['X-Midpoint SavGol'] = pd.Series(midpoint_x)
        data_csv['Y-Midpoint SavGol'] = pd.Series(midpoint_y)

        data_csv['Head Speed'] =  pd.Series(head_v)
        data_csv['Tail Speed'] =  pd.Series(tail_v)
        data_csv['Centroid Speed'] =  pd.Series(centroid_v)
        data_csv['Midpoint Speed'] =  pd.Series(midpoint_v)
        # Save the speed and the filtered x/y positions
        data_csv.to_csv(Path(i_folder, data_name))

        time = np.linspace(0, experiment_settings['Recording time'],
                           centroid_v.shape[0])
        # plot the speed for each experiment
        fig = plt.figure(figsize=(12,6))
        ax = fig.add_subplot(111)
        ax.plot(time, head_v, label='Head Speed')
        ax.plot(time, centroid_v, label='Centroid Speed')
        ax.plot(time, tail_v, label='Tail Speed')
        ax.set_ylim(0,4)
        ax.legend()
        ax.set_ylabel('Speed [mm/s]')
        ax.set_xlabel('Time [s]')
        ax.set_title(filepath.name + '\nMean Run Speed: ' + repr(np.nanmean(tail_v)))
        fig.tight_layout()
        fig.savefig(Path(i_folder, 'speed_plot.png'))