"""
This script asks the user to select either a single PiVR data folder or
a directory containing multiple PiVR data folders and then scans the
csv files containing 2d positional data of the larvae for the presence
of erroneous head and tail assignments. It uses the following algorithm – 1)
find the locations where the larva has made body-curls (i.e. extreme
bending of larval body posture), 2) check whether the head points are
leading the direction of larval motion immediately after a given
body-curling event, 3) If instead tail points are found to be leading
the direction of motion then assume an error and swap head and tail
positions in that trajectory segment. This algorithm works on the
assumption that there are no cases of backward motion (i.e. tail
leading the direction of motion) immediately after a body-curl.
While the script lacks interactive visualization, it generates an
image file containing plots of larval trajectories both before and
after the corrections in head and tail assignments.
"""

#Aim: to get into a selected PiVR folder or a selected parent folder containing multiple PiVR folders,
#access the csv file and then find out head tail swaps and then correct those errors, save a new corrected
#csv file. Note: filter the csv columns before this process. Also, save a plot of trajectories before and
# after correction.
#Author: Nitesh Saxena (nitesh@ucsb.edu), Louis lab.
#Contributors: 1) Zainab Bansfield (zbansfield@ucsb.edu) in writing and testing parts of the code.
# 2) A part of Cyrene's code on larval trajectories provided a design for accessing multiple directories (cyrenahowland@ucsb.edu),
# 3) Sid (sidharthbarathi@umail.ucsb.edu), Ishan (ishan@ucsb.edu), Shivani (shivanisista@umail.ucsb.edu) played a role in validating the performance of HT-error correction algorithm.


from pathlib import Path
import os
import pandas as pd
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import json
import tkinter.filedialog

def HTcode(pathinfo,csvdata,filename):
    # Below, I used class method to get the filtered x,y information from the csv file (convention: letter 'c','h','t' denote centroid, head and tail columns in the csv file.
    class fileIn:
        def __init__(self, file):
            self.time_per_frame = 1 / 30  # not used in the code
            #local_json_file_string = Path.joinpath('/experiment_settings.json')
            os.chdir(pathinfo)
            exp_settings = open('experiment_settings.json')  # insert path to experiment settings file
            exp_set = json.load(exp_settings)
            self.px_per_mm = exp_set["Pixel per mm"]
            if np.isnan(file.iloc[len(file)-1,3]):  # dropping the last row as it is incomplete in some files
                file = file[:-1]
            self.xcFilt = signal.savgol_filter(file.iloc[:, 3].dropna(), 57, 2)  # Caution: filter features changes binarized arrays (current filter name is savotsky-golay filter but a simple moving average filter also works fine.
            self.ycFilt = signal.savgol_filter(file.iloc[:, 4].dropna(), 57, 2)
            self.xhFilt = signal.savgol_filter(file.iloc[:, 5].dropna(), 57, 2)
            self.yhFilt = signal.savgol_filter(file.iloc[:, 6].dropna(), 57, 2)
            self.xtFilt = signal.savgol_filter(file.iloc[:, 7].dropna(), 57, 2)
            self.ytFilt = signal.savgol_filter(file.iloc[:, 8].dropna(), 57, 2)
            self.frame = file.iloc[:, 1]
            self.time = file.iloc[:, 2]
            self.xmFilt = signal.savgol_filter(file.iloc[:, 9].dropna(), 57, 2)
            self.ymFilt = signal.savgol_filter(file.iloc[:, 10].dropna(), 57, 2)
            stimulation = pd.DataFrame(file,columns = ['stimulation'])
            self.stimulation = stimulation.iloc[:,0]


    # I wrote this tiny function to call the content from the class function above
    def filter_PiVRcsvfile():
        return fileIn(csvdata)

    f = filter_PiVRcsvfile()  # file
    #print(f.px_per_mm)  

    # function to calculate "max" body length (Zainab's work)-
    def max_body_length(xhead,yhead,xtail,ytail):
        HTdist = (((xhead - xtail) ** 2 + (yhead - ytail) ** 2) ** (1 / 2))
        bodylength = max(HTdist)  # find max HT distance to set as the body length#assuming, filtered vales will not have erroneous values in the HT length values.
        return bodylength
    curl_threshold = (0.2)*max_body_length(f.xhFilt,f.yhFilt,f.xtFilt,f.ytFilt) #threshold should be less than 20% blength based on Zainab's findings.
    #print(curl_threshold)

    # this function binarized the array based on if the animal is curling (1s) or not curling (0s):
    def condition_based_binary_array(x1, y1, x2, y2):
        Dht = ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** (1 / 2)
        binarized_array = []
        for k in range(len(Dht)):
            if Dht[k] < curl_threshold:  #####NOTE: CURLING THRESHOLD SHOULD BE OBJECTIVELY DEFINED (earlier it was 3).
                binarized_array.append(1)
            else:
                binarized_array.append(0)
        # binarized_array.pop(0)
        return binarized_array

    ba = condition_based_binary_array(f.xhFilt, f.yhFilt, f.xtFilt, f.ytFilt)  # binary array for curls and no curls.

    # print(ba)
    # print(len(ba)-1)

    # Again I used class(though not necessary and a bit overkill to use class for simple tasks), to get the locations of where the curls start and ends (denoted as starts of 0s and ends of 0s etc).
    class extractInfoFromBinaryArr:
        def __init__(self, binary_arr):
            starts0 = []
            ends0 = []
            starts1 = []
            ends1 = []
            seglen0 = []
            seglen1 = []
            for i in range(len(binary_arr) - 1):
                if i == 0 and binary_arr[i] == 0:
                    starts0.append(i)
                if i == 0 and binary_arr[i] == 1:
                    starts1.append(i)
                if i == len(binary_arr) - 2 and binary_arr[i] == 0:
                    ends0.append(i + 1)
                if i == len(binary_arr) - 2 and binary_arr[i] == 1:
                    ends1.append(i + 1)
                if binary_arr[i] == 1 and binary_arr[i + 1] == 0:
                    starts0.append(i + 1)
                    ends1.append(i)
                elif binary_arr[i] == 0 and binary_arr[i + 1] == 1:
                    ends0.append(i)
                    starts1.append(i + 1)
            seglen0 = np.subtract(np.array(ends0), np.array(starts0))
            seglen1 = np.subtract(np.array(ends1), np.array(starts1))
            self.starts0 = starts0
            self.ends0 = ends0
            self.starts1 = starts1
            self.ends1 = ends1
            self.seglen0 = seglen0
            self.seglen1 = seglen1

    # same logic as above to extract information from the class method above
    def binary_arr_starts_ends():
        return extractInfoFromBinaryArr(ba)

    ba_f = binary_arr_starts_ends()  # binary array features
    # print(binary_arr_features.starts0)

    # because we need to calculate distances between two points in 2d a lot, a simple function for this small task:
    def dis(x1, y1, x2, y2):
        distance = ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** (1 / 2)
        return distance

    # below is the method which decides if there is an HT exchange error at the beginning of a given inter-curl segment. this function takes many inputs and the main function which tests for the presence of ht error is writen within this function.
    def ht_error_correction_module(filename, starts0, ends0, xc, yc, xh, yh, xt, yt, frame, time, xm, ym, stimulation):
        xheadCorrected = xh.copy()  # xh ###concern that David warned us about #late edit by Nitesh
        yheadCorrected = yh.copy()  # yh ###concern that David warned us about
        xtailCorrected = xt.copy()  # xt ###concern that David warned us about
        ytailCorrected = yt.copy()  # yt ###concern that David warned us about
        # here plotting the figure which has no correction done yet:
        fig, (ax1, ax2) = plt.subplots(1, 2)
        ax1.plot(xh, yh, 'r', label='head')
        ax1.plot(xt, yt, 'b', label='tail')
        ax1.set_xlabel('x axis')
        ax1.set_ylabel('y axis')
        ax1.set_title('trajectory before HT error correction')

        # this is the main function which tests for the presence of head tail error between two locations of the larvae (one position at the beginning of inter curl segment and second frame, 60 steps further down).
        def HTerror_corr_method1(fr1, fr2, j):
            distance1 = dis(xc[fr1], yc[fr1], xt[fr2], yt[fr2])
            distance2 = dis(xc[fr1], yc[fr1], xh[fr2], yh[fr2])
            # if error is found, exchange head and tail values thus "correcting" the ht error:
            if distance1 > distance2:
                xheadCorrected[starts0[j]:ends0[j] + 1] = xt[starts0[j]:ends0[j] + 1].copy()  ###David's concern # edit by Z: to avoid jumps, added +1 - not sure if this is necessary in all cases but the inter curl segment seems to go until the last frame but your code identified it as ending in the second to last frame.. will need to test more trajectories
                yheadCorrected[starts0[j]:ends0[j] + 1] = yt[starts0[j]:ends0[j] + 1].copy()  ###David's concern
                xtailCorrected[starts0[j]:ends0[j] + 1] = xh[starts0[j]:ends0[j] + 1].copy()  ###David's concern
                ytailCorrected[starts0[j]:ends0[j] + 1] = yh[starts0[j]:ends0[j] + 1].copy()  ###David's concern # edit by Z: changed variable (it was xtailCorrected when it have been y.. this caused the big error)

        for n in range(len(starts0)):
            HTerror_corr_method1(starts0[n], starts0[n] + 60,n)  # 60 is the number of frames or distance in frames between two larval positions to estimate whether larva has inverted or proper head tail position. this threshold can be changed.
            xheadCorrected = list(np.round(xheadCorrected, decimals=1))
            yheadCorrected = list(np.round(yheadCorrected, decimals=1))
            xtailCorrected = list(np.round(xtailCorrected, decimals=1))
            ytailCorrected = list(np.round(ytailCorrected, decimals=1))
        # plotting basic trajectories to visualize the correction:
        ax2.plot(xheadCorrected, yheadCorrected, 'r', label='head')
        ax2.plot(xtailCorrected, ytailCorrected, 'b', label='tail')
        ax2.set_xlabel('x axis')
        ax2.set_ylabel('y axis')
        ax2.set_title('trajectory after HT error correction')
        plt.suptitle('trajectory before and after HT error correction')
        fig.set_figheight(6)
        fig.set_figwidth(11)
        handles, labels = ax1.get_legend_handles_labels() #or ax2
        fig.legend(handles, labels, loc='upper left')
        fig.savefig('trajectory before and after HT error correction.png')
        #plt.show(block=False)  #to unblock the session to be able to close the window
        #plt.time.sleep(5)
        plt.close(fig)

        # filename for the corrected file:
        fileparts = filename.split('.')
        first_part = '.'.join(fileparts[0:len(fileparts) - 1])
        second_part = "_filtered_HTcorrected"
        filename_corrected = '.'.join([first_part + second_part, fileparts[-1]])  # a way to rename the corrected file
        # below is a way to name the heading (first) row of the new csv file:
        newCorrectedArray = {
            'Frame': f.frame,
            'Time': f.time,
            'xCentroid': list(np.round(xc, decimals=1)),
            'yCentroid': list(np.round(xc, decimals=1)),
            'xHeadCorrected': xheadCorrected,
            'yHeadCorrected': yheadCorrected,
            'xTailCorrected': xtailCorrected,
            'yTailCorrected': ytailCorrected,
            'xMidpoint': list(np.round(f.xmFilt, decimals=1)),
            'yMidpoint': list(np.round(f.ymFilt, decimals=1)),
            'stimulation': f.stimulation
        }
        # putting the actual data of corrected xy values in the array first:
        df = pd.DataFrame(newCorrectedArray)
        # and then put it in a csv of the new filename:
        df.to_csv(filename_corrected)
        return xheadCorrected, yheadCorrected, xtailCorrected, ytailCorrected

    #xhC, yhC, xtC, ytC = ht_error_correction_module(filename, ba_f.starts0, ba_f.ends0, f.xcFilt, f.ycFilt, f.xhFilt, f.yhFilt, f.xtFilt, f.ytFilt)
    ht_error_correction_module(filename, ba_f.starts0, ba_f.ends0, f.xcFilt, f.ycFilt, f.xhFilt, f.yhFilt, f.xtFilt, f.ytFilt, f.frame, f.time, f.xmFilt, f.ymFilt, f.stimulation)
                                                    

##This module below was developed from a file-access module of Cyrene's code for plotting
# larval trajectories, in Data Analysis folder (Git repository) and was added just to make the
# above ht error correction code (the whole code is now put inside a function called HTcode)
# to work with directories - either single PiVR directory or a parent folder with multiple PiVR
# directories, chosen by the user)
selectedpath = tkinter.filedialog.askdirectory(title='Select a PiVR folder for single file ht correction OR a parent folder with multiple PiVR folders inside')
basepath = Path(selectedpath)
#basepath = Path('C:/PycharmProjects/head tail errors/HTerror_correction_DATA/ht_test_May2021/2018.12.18_12-14-53_MS398XMS028')
folders = [x for x in basepath.iterdir() if x.is_dir()]
if len(folders) != 0:  #suggests the path chosen was a parent folder with multiple PiVR folders inside.
    for folder_name in folders:
        print(folder_name.name)
        files = [y for y in folder_name.iterdir() if y.is_file()] #print(folder_name.parts[-1])# equivalent to #print(folder_name.name)
        for f in files:
            if 'data.csv' in f.parts[-1]:
                print(f.name)
                data = pd.read_csv(f)
                HTcode(folder_name,data,f.name)
elif len(folders) == 0:   #suggests the path chosen was a single PiVR folder.
    files = [y for y in basepath.iterdir() if y.is_file()]  # print(folder_name.parts[-1])# equivalent to #print(folder_name.name)
    for f in files:
        if 'data.csv' in f.parts[-1]:
            print(f.name)
            data = pd.read_csv(f)
            HTcode(basepath,data,f.name)





